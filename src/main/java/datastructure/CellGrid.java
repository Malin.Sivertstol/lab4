package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    int rows;
    int cols;
    CellState[][] grid;


    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this.cols = columns;
        grid = new CellState[rows][columns];

	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return this.rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.cols;
    }

    public boolean checkIndexValue(int row, int column) {
        return row >= 0 && row < numRows() && column >= 0 && column < numColumns();
    }


    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if (checkIndexValue(row, column)) {
            grid[row][column] = element;
        } else {
            throw new IndexOutOfBoundsException();
        }

    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if (checkIndexValue(row, column)) {
            return grid[row][column];
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        IGrid gridCopy = new CellGrid(numRows(), numColumns(), CellState.DEAD);

        for (int row = 0; row < numRows(); row++) {
            for (int column = 0; column < numColumns(); column++) {
                gridCopy.set(row, column, get(row, column));
            }
        }
        return gridCopy;
    }

}

