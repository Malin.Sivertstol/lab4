package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton{

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else if (random.nextBoolean()){
                    currentGeneration.set(row, col, CellState.DEAD);
                }else{
                    currentGeneration.set(row,col,CellState.DYING);
                }
            }
        }
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        for (int r=0; r<nextGeneration.numRows(); r++){
            for (int c=0; c<nextGeneration.numColumns(); c++){
                currentGeneration.set(r,c, getNextCell(r,c));
            }
        }

    }

    @Override
    public CellState getNextCell(int row, int col) {
        CellState currentCellstate = currentGeneration.get(row, col);
        int count = countNeighbors(row, col, CellState.ALIVE);
        if (getCellState(row, col) == CellState.ALIVE) {
            return CellState.DYING;
        }
        else if(getCellState(row,col) == CellState.DYING){
            return CellState.DEAD;
        }
        else if(getCellState(row, col)== CellState.DEAD){
            if (count == 2){
                return CellState.ALIVE;
            }
            else{
                return CellState.DEAD;
            }
        }
        return currentCellstate;
    }

    private int countNeighbors(int row, int col, CellState state) {
        int count = 0;

        for (int i = row - 1; i <= row + 1; i++){
            for (int n = col - 1; n <= col + 1; n++){
                if (n >= numberOfColumns()) {
                    continue;
                }
                else if (n < 0) {
                    continue;
                }
                else if (i >= numberOfRows()) {
                    continue;
                }
                else if (i < 0) {
                    continue;
                }
                else if (n == col && i == row) {
                    continue;
                }
                if (currentGeneration.get(i, n) == state) {
                    count++;
                }
            }
        }
        return count;
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}